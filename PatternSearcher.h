#pragma once

#include <deque>

#include "common.h"

namespace mtfind {

//functional class presents pattern seeker
class PatternSearcher {
public:
	//takes reference to list of matchers, pattern and incoming queue
	PatternSearcher(std::deque<Match> &matches,
			const std::regex &pattern,
			LinesQueue &queue):
		m_matches(matches), m_pattern(pattern), m_lines_queue(queue) {}
	void operator() () {
	    LinesBuffer *lines;
	    while (!m_lines_queue.no_data) {
	        while (m_lines_queue.queue.pop(lines)) {
	            search(*lines);
	            delete lines;
	        }
	    }

        while (m_lines_queue.queue.pop(lines)) {
            search(*lines);
            delete lines;
        }
	}

private:
	void search(LinesBuffer &lines) {
		std::smatch results;
		for(auto &l: lines) {
			unsigned first_pos = 0;
			while(std::regex_search(l.line, results, m_pattern)) {
				for(auto &result: results) {
					unsigned pos = result.first - l.line.begin() + first_pos;
					first_pos += result.second - l.line.begin();
					m_matches.push_back({l.line_number, pos, result.str()});
				}
				l.line = results.suffix();
			}
		}


		//m_matches.push_back({line.line_number, 0, ""});
	}

	std::deque<Match> &m_matches;
	const std::regex &m_pattern;
	LinesQueue &m_lines_queue;
};

} // end of mtfind
