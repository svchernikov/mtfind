/*
 * utils.h
 *
 *  Created on: Jun 23, 2018
 *      Author: sergey
 */

#pragma once

#include <vector>
#include <numeric>

#include "common.h"

namespace mtfind {

inline std::string usage() {
	return std::string(R"(Usage:
mtfind <text.file> <pattern>)");
}

//escape special symbols and replace "?" to ".{1}"
template <typename stringT>
stringT prepare_pattern(const stringT &pattern) {
	std::regex to_escape(R"([-[\]{}()*+.,\^$|#\s])");
	std::regex to_modify(R"([?])");

	auto escaped = std::regex_replace(pattern, to_escape, R"(\$&)");

	return std::regex_replace(escaped, to_modify, R"(.{1})");
}

//merge matches
//has O(n) complexity because each list is already ordered
template <typename containerT>
auto merge_matches(containerT &matches_lists) {
	typename containerT::value_type sorted_list;

	using iter_t = typename containerT::value_type::iterator;
	//using move_iter_t = std::move_iterator<iter_t>;

	unsigned N = matches_lists.size();
	std::vector<iter_t> lists_iters(N);
	unsigned empty_lists_count = 0;

	for(unsigned ti = 0; ti < N; ++ti) {
		lists_iters[ti] = begin(matches_lists[ti]);
		if(lists_iters[ti] == end(matches_lists[ti]))
			++empty_lists_count;
	}

	while(empty_lists_count != N) {
		unsigned min_ti = 0;
		unsigned min_line_number = std::numeric_limits<unsigned>::max();
		for(unsigned ti = 0; ti < N; ++ti) {
			auto it = lists_iters[ti];
			if(it != end(matches_lists[ti])) {
				if(it->line_number < min_line_number) {
					min_ti = ti;
					min_line_number = it->line_number;
				}
			}
		}
		auto &min_it = lists_iters[min_ti];
		sorted_list.push_back(std::move(*min_it));
		if(++min_it == end(matches_lists[min_ti]))
			++empty_lists_count;
	}
	return sorted_list;
}

} //end of mtfind
