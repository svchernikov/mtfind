/*
 * mtfind.h
 *
 *  Created on: Jun 23, 2018
 *      Author: sergey
 */

#pragma once

#include <stdexcept>

#include "common.h"

namespace mtfind {

//check command line arguments
void check_parameters(int argc, char **argv);

//main function to pattern search
std::deque<Match> pattern_search(std::istream &ist,
					const std::string &pattern,
					int n_threads = -1);

} //end of mtfind
