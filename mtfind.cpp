/*
 * mtfind.cpp
 *
 *  Created on: Jun 23, 2018
 *      Author: sergey
 */

#include "common.h"
#include "utils.h"
#include "PatternSearcher.h"

#include <vector>
#include <deque>
#include <algorithm>

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>

namespace mtfind {

void check_parameters(int argc, char **argv) {
	using namespace boost::filesystem;

	//1. Check number of arguments
	if(argc < 3) {
		throw std::runtime_error("The program takes at least two parameters.\n" + usage());
	}
	std::string filename(argv[1]);
	std::string pattern(argv[2]);

	//2. Try to find "\n" symbol
	if(pattern.find("\n\r") != std::string::npos) {
		throw std::runtime_error("The pattern cannot contain '\\n' and '\\r' symbols.\n");
	}

	//3. Check pattern length
	if(pattern.length() > 100) {
		throw std::runtime_error("Length of the pattern should not be more then 100.\n");
	}

	//4. The input file exists and it is regular
	if(!exists(filename) || !is_regular(filename)) {
		throw std::runtime_error("The input file doesn't exist or not regular.\n");
	}

	//5. Check size of input file
	if(file_size(filename) > 1024L * 1024L * 1024L) {
		throw std::runtime_error("The input file is too large (>1GB).\n");
	}
}



std::deque<Match> pattern_search(std::istream &ist,
					const std::string &pattern,
					int n_threads) {


	//get number of threds
	n_threads = (n_threads > 1)?n_threads:(boost::thread::hardware_concurrency() + 1);

	//prepare regex pattern
	std::regex re_pattern(prepare_pattern(pattern));

	//input queue
	LinesQueue lines_queue((n_threads - 1) * QUEUE_SIZE_PER_THREAD);

	//output list of matches for every thread
	std::vector<std::deque<Match>> matches_lists(n_threads - 1);

	//create threads
	boost::thread_group  search_threads;
	for(int ti = 0; ti < n_threads - 1; ++ti)
		search_threads.create_thread(PatternSearcher(matches_lists[ti], re_pattern, lines_queue));

	//go through the file
	for(unsigned li = 1; !ist.eof();) {
		std::string line;
		LinesBuffer *buf = new LinesBuffer;
		for(unsigned bi = 0; bi < buf->size() && !ist.eof(); ++bi, ++li) {
			std::getline(ist, line);
			(*buf)[bi].line = std::move(line);
			(*buf)[bi].line_number = li;
		}

		while (!lines_queue.queue.push(buf)) {}
	}
	lines_queue.no_data = true;
	search_threads.join_all();

	return merge_matches(matches_lists);
}

}


