/*
 * Common.h
 *
 *  Created on: Jun 23, 2018
 *      Author: sergey
 */
#pragma once

#include <string>
#include <regex>
#include <iostream>
#include <algorithm>
#include <array>

#include <boost/lockfree/queue.hpp>
#include <boost/atomic.hpp>

namespace mtfind {

constexpr unsigned QUEUE_SIZE_PER_THREAD = 9192;
constexpr unsigned MAX_QUEUE_SIZE = 65533;
constexpr unsigned LINES_BUFFER_SIZE = 1024;

//incoming line
struct Line {
	unsigned line_number;
	std::string line;
};

using LinesBuffer = std::array<Line, LINES_BUFFER_SIZE>;


//lock-free queue + eof flag
struct LinesQueue {
	LinesQueue(unsigned size): queue(std::min(size, MAX_QUEUE_SIZE)), no_data(false) {}
	//lock-free queue for incoming lines from a file
	boost::lockfree::queue<LinesBuffer*, boost::lockfree::fixed_sized<true>> queue;
	//becomes true if end of file
	boost::atomic<bool> no_data;
};


//data for output
struct Match {
	unsigned line_number;
	unsigned start_pos;
	std::string subline;
};

}
