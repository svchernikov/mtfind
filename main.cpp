/*
 * main.cpp
 *
 *  Created on: Jun 23, 2018
 *      Author: sergey
 */

#include "mtfind.h"

#include <fstream>
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;
using std::flush;


int main(int argc, char **argv) {
	if(argc < 3) {
		cerr << "The program takes at least two arguments:\n"
				"Usage:\n"
				"mtfind <text.file> <pattern>"<< endl;
		return 1;
	}

	std::ifstream txt_file;
	//check parameters
	try {
		mtfind::check_parameters(argc, argv);
		txt_file.open(argv[1]);
		if(!txt_file) {
			throw std::runtime_error("The input file cannot be opened.\n");
		}
	} catch(std::runtime_error &err) {
		cerr << "An error has occurred:\n" << err.what() << endl;
		return -1;
	}

	auto matches = mtfind::pattern_search(txt_file, argv[2]);

	//print results
	cout << matches.size() << endl;
	for(auto &m: matches) {
		cout << m.line_number << " "  << m.start_pos + 1 << " " << m.subline << "\n";
	}
	cout << flush;

	return 0;
}

